import javafx.scene.control.*;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import java.io.IOException;

import static org.testfx.assertions.api.Assertions.assertThat;

@ExtendWith(ApplicationExtension.class)
public class DummyTest extends FxTest {
    public static final String PATH_TO_FXML = "/layout/sample.fxml";
    private Button buttonPrintHelloWorld;

    @Start
    void initializeGUI(Stage primaryStage) throws IOException {
        startApplication(primaryStage, PATH_TO_FXML);
        buttonPrintHelloWorld = getById("buttonPrintHelloWorld");
    }

    @Test
    void initialState_button_is_active() {
        assertThat(buttonPrintHelloWorld.isVisible());
    }


}