package presentation;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class SampleController implements Initializable {
    @FXML
    private Button buttonPrintHelloWorld;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        buttonPrintHelloWorld.setOnAction((event) -> System.out.println("Hello World!"));
    }
}
